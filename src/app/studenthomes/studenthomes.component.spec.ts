import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudenthomesComponent } from './studenthomes.component';

describe('StudenthomesComponent', () => {
  let component: StudenthomesComponent;
  let fixture: ComponentFixture<StudenthomesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudenthomesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudenthomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
