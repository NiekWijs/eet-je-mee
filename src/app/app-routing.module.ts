import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { AboutComponent } from './about/about.component';
import { UserComponent } from './user/user.component';
import { MealsComponent } from './meals/meals.component';
import { StudenthomesComponent } from './studenthomes/studenthomes.component';
import { RecipesComponent} from './recipes/recipes.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: 'recipes', pathMatch: 'full', component: RecipesComponent},
      { path: 'user', pathMatch: 'full', component: UserComponent },
      { path: 'meals', pathMatch: 'full', component: MealsComponent },
      { path: 'studenthomes', pathMatch: 'full', component: StudenthomesComponent },
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
      { path: 'about', pathMatch: 'full', component: AboutComponent },
    ],
  },
  {path: 'login', pathMatch: 'full', component: LoginComponent},
  {path: 'register', pathMatch: 'full', component: RegisterComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
